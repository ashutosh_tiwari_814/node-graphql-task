1. add admin
mutation{
	createAdmin(
    adminInput:{email:"admin1@gmail.com", password:"12345678"}),
 {_id,email}
}
  

2. login admin
query{
	login(email:"admin@gmail.com", password:"12345678"){
		adminId,token
  }
}
  

3. create product 
mutation {
  createProduct(productInput: {title: "Hp laptop", price: 45550.00, description: "HP NoteBook is a Windows 10 laptop with a 15.60-inch display that has a resolution of 1366x768 pixels. It is powered by a Core i5 processor and it comes with 8GB of RAM. The HP NoteBook packs 256GB of SSD storage. Graphics are powered by Intel HD Graphics 620."}) {
    _id
    title
    description
    price
    createdAt
  }
}


4. delete product
mutation {
 deleteProduct(productId:"6242ef1b493efc70d49de16a")
}


5. update product
mutation {
  updateProduct(productInput: {productId: "6242f26b580a5408ec132056", title: "Dell"}) {
    _id
    title
    price
    createdAt
  }
}

6. product list 
query{
	products{
    _id,title,description,price,createdAt
  }
}
