const { buildSchema } = require("graphql");

module.exports = buildSchema(`


type Product {
  _id: ID!
  title: String!
  description: String
  price: Float!
  createdAt: String!
}

type Admin {
  _id: ID!
  email: String!
  password: String 
}

type AuthData {
  adminId: ID!
  token: String! 
}

input ProductInput {
  productId: String
  title: String!
  price: Float!
  description: String
}
input ProductUpdate {
  productId: String!
  title: String
  price: Float
  description: String
}

input AdminInput {
  email: String!
  password: String!
}

type RootQuery {
    products: [Product!]!
    login(email: String!, password: String!): AuthData!
}

type RootMutation {
    createProduct(productInput: ProductInput): Product
    createAdmin(adminInput: AdminInput): Admin
    updateProduct(productInput: ProductUpdate): Product!
    deleteProduct(productId: ID!): String
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`);
