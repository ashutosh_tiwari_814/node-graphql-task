const ProductModel = require("../../models/product");

const { transformProduct } = require("./merge");

module.exports = {
  products: async () => {
    try {
      const products = await ProductModel.find();
      return products.map((product) => {
        return transformProduct(product);
      });
    } catch (err) {
      throw err;
    }
  },
  createProduct: async (args, req) => {
    // if (!req.isAuth) {
    //   throw new Error("Unauthenticated!");
    // }
    const product = new ProductModel({
      title: args.productInput.title,
      description: args.productInput.description,
      price: +args.productInput.price,
    });
    let createdProduct;
    try {
      const result = await product.save();
      createdProduct = transformProduct(result);
      return createdProduct;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
  deleteProduct: async (args, req) => {
    // if (!req.isAuth) {
    //   throw new Error("Unauthenticated!");
    // }
    try {
      await ProductModel.findByIdAndDelete(args.productId);
      return "";
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
  updateProduct: async (args, req) => {
    // if (!req.isAuth) {
    //   throw new Error("Unauthenticated!");
    // }
    // const updateProduct = {
    //   title: args.productInput.title,
    //   description: args.productInput.description,
    //   price: +args.productInput.price,
    // };
    try {
      console.log(args.productInput);
      const result = await ProductModel.findByIdAndUpdate(
        args.productInput.productId,
        { $set: args.productInput },
        { new: true }
      );
      updatedProduct = transformProduct(result);
      return updatedProduct;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
};
