const ProductModel = require("../../models/product");
const AdminModel = require("../../models/admin");
const { dateToString } = require("../../helpers/date");

const products = async (productIds) => {
  try {
    const products = await ProductModel.find({ _id: { $in: productIds } });
    return products.map((product) => {
      return transformEvent(product);
    });
  } catch (err) {
    throw err;
  }
};

const singleProduct = async (productId) => {
  try {
    const product = await ProductModel.findById(productId);
    return transformEvent(product);
  } catch (err) {
    throw err;
  }
};

const admin = async (adminId) => {
  try {
    const admin = await AdminModel.findById(adminId);
    return {
      ...admin._doc,
      _id: admin.id,
    };
  } catch (err) {
    throw err;
  }
};

const transformProduct = (product) => {
  return {
    ...product._doc,
    _id: product.id,
    createdAt: dateToString(product._doc.createdAt),
  };
};

exports.transformProduct = transformProduct;
