const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_SECRET, JWT_TOKEN_EXPIRE } = require("../../constant");

const AdminModel = require("../../models/admin");

module.exports = {
  // admin creation
  createAdmin: async (args) => {
    try {
      const existingAdmin = await AdminModel.findOne({
        email: args.adminInput.email,
      });
      if (existingAdmin) {
        throw new Error("Admin exists already.");
      }
      const hashedPassword = await bcrypt.hash(args.adminInput.password, 12);

      const admin = new AdminModel({
        email: args.adminInput.email,
        password: hashedPassword,
      });

      const result = await admin.save();

      return { ...result._doc, password: null, _id: result.id };
    } catch (err) {
      throw err;
    }
  },

  // admin login
  login: async ({ email, password }) => {
    const admin = await AdminModel.findOne({ email: email });
    if (!admin) {
      throw new Error("Admin does not exist!");
    }
    const isEqual = await bcrypt.compare(password, admin.password);
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    const token = jwt.sign(
      { adminId: admin._id, email: admin.email },
      JWT_SECRET,
      {
        expiresIn: JWT_TOKEN_EXPIRE,
      }
    );
    return { adminId: admin.id, token: token, tokenExpiration: 1 };
  },
};
